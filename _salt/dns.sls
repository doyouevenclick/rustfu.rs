#!pyobjects

def record(host, type, value):
    BotoRoute53.present(
        "%s_%s" % (host, type),
        name=host,
        value=value,
        zone="rustfu.rs",
        record_type=type,
        ttl=(60*60),
    )


def alias(target, source, nameserver=None):
    ip4 = salt.dnsutil.A(source, nameserver=nameserver)
    if ip4 and not isinstance(ip4, str):
        record(target, "A", ip4)

    ip6 = salt.dnsutil.AAAA(source, nameserver=nameserver)
    if ip6 and not isinstance(ip6, str):
        record(target, "AAAA", ip6)

with BotoRoute53.hosted_zone_present(
    "rustfu.rs.",
    domain_name="rustfu.rs.",
    comment="",
):
  alias("rustfu.rs", "angry-blackwell-93ebf8.netlify.com")
  record("www.rustfu.rs", "CNAME", "angry-blackwell-93ebf8.netlify.com")
  record("wiki.rustfu.rs", "CNAME", "condescending-bhaskara-d5a97a.netlify.com")
